#!/usr/bin/make -f

UPSTREAM_GIT = https://github.com/openvswitch/ovs
include /usr/share/openstack-pkg-tools/pkgos.make

export DEB_BUILD_MAINT_OPTIONS=hardening=+bindnow

%:
	dh $@ --with autoreconf,python3,sphinxdoc

# use --as-needed only if supported by dh-autoreconf (to simplify backporting)
DH_AS_NEEDED=$(shell dpkg --compare-versions $$(dpkg --status dh-autoreconf | grep Version | cut -d' ' -f2) ge 6 && echo --as-needed)
override_dh_autoreconf:
	dh_autoreconf $(DH_AS_NEEDED)

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
PARALLEL = -j$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
else
PARALLEL =
endif

override_dh_auto_configure:
	test -d _debian || mkdir _debian
	cd _debian && ( \
		test -e Makefile || \
		../configure --prefix=/usr --localstatedir=/var --enable-ssl --enable-shared \
			--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
			--sysconfdir=/etc $(DATAPATH_CONFIGURE_OPTS))
ifneq (,$(filter i386 amd64 ppc64el arm64, $(DEB_HOST_ARCH)))
	test -d _dpdk || mkdir _dpdk
	cd _dpdk && ( \
		test -e Makefile || \
		../configure --prefix=/usr --localstatedir=/var --enable-ssl --enable-shared \
			--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
			--with-dpdk=shared --sysconfdir=/etc \
			$(DATAPATH_CONFIGURE_OPTS))
endif


# NOTE(zigo): by default, just run all tests
# but 20: bfd - bfd decay FAILED (bfd.at:396)
# that one doesn't feel like deterministic.
# 1021: ofproto-dpif - select group with weights FAILED (ofproto-dpif.at:535)
# 1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
TEST_LIST = 1-19 21-531 533-1020 1022-1056 1058-
TEST_LIST_DPDK = $(TEST_LIST)

# armhf:
#   24: bfd.at:662         bfd - check that BFD works together with RSTP
#   25: bfd - check that BFD works together with RSTP   FAILED (bfd.at:829)
#   26: bfd - check that BFD works together with RSTP   FAILED (bfd.at:829)
#  918: ofproto - asynchronous message control (OpenFlow 1.3) ok
# 1021: ofproto-dpif - select group with weights        FAILED (ofproto-dpif.at:535)
# 1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
ifneq (,$(filter armhf, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-23 28-531 533-917 919-1020 1022-1056 1058-
TEST_LIST_DPDK = $(TEST_LIST)
endif # armhf

# mipsel:
#    20: bfd - bfd decay FAILED (bfd.at:396)
#    21: bfd - bfd decay
#   917: ofproto - asynchronous message control (OpenFlow 1.2) (ofproto.at:3183)
#   918: ofproto - asynchronous message control (OpenFlow 1.3) FAILED (ovs-macros.at:241)
#   927: ofproto - asynchronous message control (OpenFlow 1.3)  FAILED (ofproto.at:3405)
#   919: ofproto - asynchronous message control (OpenFlow 1.4) FAILED (ovs-ofctl)
#  1035: ofproto-dpif - select group with weights
#  1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
#  1069: ofproto-dpif - controller action without megaflows
#  1102: ofproto-dpif - continuation - resubmit FAILED (ovs-macros.at:241)
#  1071: ofproto-dpif - controller action without megaflows
#  1136: ofproto-dpif - sFlow packet sampling - LACP structures
ifneq (,$(filter mipsel, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 22-531 533-916 920-926 928-1020 1022-1034 1036-1056 1058-1068 1070 1072-1101 1103-1135 1137-
TEST_LIST_DPDK = $(TEST_LIST)
endif # mipsel

# mips64el:
#    20: bfd - bfd decay FAILED (bfd.at:396)
#  1033: ofproto-dpif - select group with weights
#  1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
#  1021: ofproto-dpif - select group with weights        FAILED (ofproto-dpif.at:535)
#  1069: ofproto-dpif - controller action without megaflows
ifneq (,$(filter mips64el, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 22-531 533-1020 1022-1032 1034-1056 1058-1068 1070 1072-1120 1122-
TEST_LIST_DPDK = $(TEST_LIST)
endif # mips64el

# armel:
#  1021: ofproto-dpif - select group with weights        FAILED (ofproto-dpif.at:535)
#  1123: ofproto-dpif - sFlow packet sampling - LACP structures FAILED (ofproto-dpif.at:6643)
ifneq (,$(filter armel, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-531 533-1020 1022-1056 1058-1122 1124-
TEST_LIST_DPDK = $(TEST_LIST)
endif #armel

# arm64:
#   159: ofp-actions - inconsistent MPLS actions         FAILED (ofp-actions.at:819)
#  1021: ofproto-dpif - select group with weights        FAILED (ofproto-dpif.at:535)
#  1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
ifneq (,$(filter arm64, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-158 160-531 533-1020 1022-1056 1058-
TEST_LIST_DPDK = $(TEST_LIST)
endif #arm64

# alpha
#  2724: ovn -- dns lookup : 1 HV, 2 LS, 2 LSPs/LS       FAILED (ovn.at:7451)
#  2728: ovn -- vlan traffic for external network with distributed router gateway port FAILED (ovn.at:8563)
#  2737: ovn -- IPv6 periodic RA                         FAILED (ovn.at:9916)
ifneq (,$(filter alpha, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-531 533-2723 2725-2727 2729-2736 2738-
TEST_LIST_DPDK = $(TEST_LIST)
endif #alpha

# hppa
#    20: bfd - bfd decay FAILED (bfd.at:314)
#   847: ofproto - bundle del group (OpenFlow 1.5) FAILED (ofproto.at:813)
#   850: ofproto - bundle remove group buckets FAILED (ofproto.at:1005)
#   916: ofproto - asynchronous message control (OpenFlow 1.0) FAILED (ovs-macros.at:241)
#   917: ofproto - asynchronous message control (OpenFlow 1.2) FAILED (ovs-macros.at:241)
#   919: ofproto - asynchronous message control (OpenFlow 1.4) FAILED (ovs-ofctl)
#   918: ofproto - asynchronous message control (OpenFlow 1.3) FAILED (ofproto.at:3371)
#   942: ofproto - flow monitoring pause and resume FAILED (ofproto.at:4850)
#  1033: ofproto-dpif - masked set-field into metadata FAILED (ofproto-dpif.at:860)
#  1021: ofproto-dpif - select group with weights FAILED (ofproto-dpif.at:535)
#  1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
#  1102: ofproto-dpif - continuation - resubmit FAILED (ovs-macros.at:241)
#  1105: ofproto-dpif - continuation - mirroring FAILED (ovs-macros.at:241)
#  1123: ofproto-dpif - sFlow packet sampling - LACP structures FAILED (ofproto-dpif.at:6643)
#  1181: ofproto - bundle with variable bfd/cfm config   FAILED (ofproto-dpif.at:8749)
#  1209: ofproto-dpif - conntrack - zones                FAILED (ofproto-dpif.at:9898)
#  1735: ovsdb-server combines updates on backlogged connections FAILED (ovsdb-server.at:1205)
#  2193: RSTP - dummy interface FAILED (rstp.at:210)
ifneq (,$(filter hppa, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-531 533-846 848-849 851-915 920-941 943-1020 1022-1056 1058-1101 1103-1104 1106-1122 1124-1180 1182-1208 1210-1734 1736-2192 2194-
TEST_LIST_DPDK = $(TEST_LIST)
endif #hppa

# sparc64
#    20: bfd - bfd decay                                 FAILED (bfd.at:396)
#    26: bfd - check that BFD works together with RSTP   FAILED (bfd.at:829)
#   466: fuzz regression - ofp_print_fuzzer-5395207246839808 FAILED (fuzz-regression.at:12)
#   900: ofproto - bundle packet-out makes bundle commit to fail(OpenFlow 1.4) FAILED (ofproto.at:2180)
#   918: ofproto - asynchronous message control (OpenFlow 1.3) ok
#   942: ofproto - flow monitoring pause and resume      ok
#   998: PMD - monitor threads                           FAILED (pmd.at:660)
#  1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
#  2184: STP - dummy interface                           FAILED (stp.at:439)
#  2185: STP - flush the fdb and mdb when topology changed FAILED (stp.at:529)
#  2249: auto-attach - packets                           FAILED (auto-attach.at:5)
ifneq (,$(filter sparc64, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-25 27-465 467-531 533-899 901-917 919-941 943-997 999-1056 1058-2183 2186-2248 2250-
TEST_LIST_DPDK = $(TEST_LIST)
endif #sparc64

# ia64
#    20: bfd - bfd decay                                 FAILED (bfd.at:396)
#  1021: ofproto-dpif - select group with weights        FAILED (ofproto-dpif.at:535)
#  1057: ofproto-dpif - controller action without megaflows FAILED (ofproto-dpif.at:1893)
ifneq (,$(filter ia64, $(DEB_HOST_ARCH)))
TEST_LIST = 1-19 21-531 533-1020 1022-1056 1058-
TEST_LIST_DPDK = $(TEST_LIST)
endif #ia64

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	if $(MAKE) -C _debian check TESTSUITEFLAGS='$(PARALLEL) $(TEST_LIST)' || \
		$(MAKE) -C _debian check TESTSUITEFLAGS='--recheck' ; then :; \
	else \
		cat _debian/tests/testsuite.log; \
		exit 1; \
	fi
ifneq (,$(filter i386 amd64 ppc64el arm64, $(DEB_HOST_ARCH)))
	if $(MAKE) -C _dpdk check TESTSUITEFLAGS='$(PARALLEL) $(TEST_LIST_DPDK)' || \
		$(MAKE) -C _dpdk check TESTSUITEFLAGS='--recheck' ; then :; \
	else \
		cat _dpdk/tests/testsuite.log; \
		exit 1; \
	fi
endif # i386/amd64/ppc64el/arm64
endif # nocheck

override_dh_auto_build:
	set -e ; set -x ; for MYMAINTSCRIPT in openvswitch-common.postinst openvswitch-switch-dpdk.postinst ; do \
		sed s/%%MULTIARCH_TRIPLETT%%/$$(dpkg-architecture -qDEB_HOST_MULTIARCH)/ debian/$$MYMAINTSCRIPT.in >debian/$$MYMAINTSCRIPT ; \
	done

	$(MAKE) $(PARALLEL) -C _debian dist distdir=openvswitch
	$(MAKE) $(PARALLEL) -C _debian
ifneq (,$(filter i386 amd64 ppc64el arm64, $(DEB_HOST_ARCH)))
	$(MAKE) $(PARALLEL) -C _dpdk
endif

override_dh_auto_clean:
	find . -name "*.pyc" -delete
	dh_auto_clean
	set -e ; make python/ovs/version.py && cd python && python3 setup.py clean ; rm -f ovs/version.py ; cd ..
	rm -rf _debian _dpdk
	rm -f debian/openvswitch-common.postinst debian/openvswitch-switch-dpdk.postinst

override_dh_auto_install-arch:
	$(MAKE) -C _debian DESTDIR=$(CURDIR)/debian/tmp install

	# Move ovs-vswitchd away, so we can use update-alternatives
	mkdir -p $(CURDIR)/debian/openvswitch-common/usr/lib/openvswitch-common
	mv $(CURDIR)/debian/tmp/usr/sbin/ovs-vswitchd \
		$(CURDIR)/debian/openvswitch-common/usr/lib/openvswitch-common/ovs-vswitchd
	mv $(CURDIR)/debian/tmp/usr/lib/*/libopenvswitch-2.15.so.0.0.0 \
		$(CURDIR)/debian/openvswitch-common/usr/lib/openvswitch-common/libopenvswitch-2.15.so.0.0.0

override_dh_auto_install-indep:
	$(MAKE) -C _debian DESTDIR=$(CURDIR)/debian/tmp install
	set -e && for pyvers in $(PYTHON3S); do \
		cd python && python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/python3-openvswitch; cd ..; \
	done
	rm -rf $(pwd)/debian/python*/usr/lib/python*/dist-packages/*.pth

override_dh_install:
	install -D -m 0644 utilities/ovs-vsctl-bashcomp.bash $(CURDIR)/debian/openvswitch-switch/usr/share/bash-completion/completions/ovs-vsctl
	dh_install --exclude=usr/sbin/ovs-vswitchd --exclude=usr/lib/`dpkg-architecture -qDEB_HOST_MULTIARCH`/libopenvswitch-2.15.so.0.0.0

	rm -f $(CURDIR)/debian/tmp/usr/lib/*/*.la
	dh_installman --language=C
	rm -rf $(CURDIR)/debian/tmp/usr/share/man

	# remove the files managed via update-alternatives
	rm -f $(CURDIR)/debian/tmp/usr/sbin/ovs-vswitchd
	rm -f $(CURDIR)/debian/tmp/usr/lib/*/libopenvswitch-2.15.so.0.0.0

	dh_missing --fail-missing
	# openvswitch-switch
	mkdir -p debian/openvswitch-switch/usr/share/openvswitch/switch
	cp debian/openvswitch-switch.template debian/openvswitch-switch/usr/share/openvswitch/switch/default.template

ifneq (,$(filter i386 amd64 ppc64el arm64, $(DEB_HOST_ARCH)))
	install -v -D _dpdk/vswitchd/.libs/ovs-vswitchd \
		$(CURDIR)/debian/openvswitch-switch-dpdk/usr/lib/openvswitch-switch-dpdk/ovs-vswitchd-dpdk
	install -v -D _dpdk/lib/.libs/libopenvswitch-2.15.so.0.0.0 \
		$(CURDIR)/debian/openvswitch-switch-dpdk/usr/lib/openvswitch-switch-dpdk/libopenvswitch-2.15.so.0.0.0
endif

override_dh_installinit:
	dh_installinit --restart-after-upgrade
	dh_installinit -popenvswitch-switch --name=ovsdb-server --no-start
	dh_installinit -popenvswitch-switch --name=ovs-vswitchd --no-start

override_dh_installsystemd:
	dh_installsystemd -popenvswitch-switch --name=ovsdb-server
	dh_installsystemd -popenvswitch-switch --name=ovs-vswitchd
	dh_installsystemd --restart-after-upgrade -Xovs-vswitchd.service -Xovsdb-server.service

override_dh_strip:
	dh_strip --dbg-package=openvswitch-dbg

override_dh_shlibdeps:
	dh_shlibdeps -l$(CURDIR)/_debian/lib/.libs

override_dh_installman:
	echo "Do nothing..."

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
	dh_python3 -popenvswitch-ipsec  /usr --shebang=/usr/bin/python3
	dh_python3 -popenvswitch-vtep   /usr --shebang=/usr/bin/python3
	dh_python3 -popenvswitch-switch /usr --shebang=/usr/bin/python3
	dh_python3 usr/share/openvswitch/scripts --shebang=/usr/bin/python3
